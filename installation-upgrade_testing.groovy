def modes=[]
def themes=[]

node('devops') {
    modes = params["MODES"].split(",")
    themes = params["THEMES"].split(",")
}

pipeline{

    agent { label 'devops' }

    parameters{
        checkboxParameter(name: 'MODES', format: 'JSON', pipelineSubmitContent: '{"CheckboxParameter": [{"key": "Install","value": "install"},{"key": "Upgrade testing","value": "upgrade"}]}', description: 'Choose the modes in which the pipeline will run.')
        checkboxParameter(name: 'THEMES', format: 'JSON', pipelineSubmitContent: '{"CheckboxParameter": [{"key": "Clean installation","value": "clean_install"},{"key": "Dark theme installation","value": "dark_theme"}]}', description: 'Choose the theme you want to install.')
        booleanParam(name: "COMPOSER_NOT_INSTALLED", defaultValue: "false", description: "Check if your site doesn't have already a composer installed." )
        string(name: "Project_name", defaultValue: "drupal", description: "The name of the project." )
        string(name: "Drupal_version", defaultValue: "9", description: "Version of Drupal." )
        string(name: "PHP_version", defaultValue: "8", description: "Version of PHP." )
    }

    stages{

        stage(""){
            when { expression { return params.COMPOSER_NOT_INSTALLED } }

            steps{
                script{
                    sh """
                        php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
                        php -r "if (hash_file('sha384', 'composer-setup.php') === '906a84df04cea2aa72f40b5f787e49f22d4c2f19492ac310e8cba5b96ac8b64115ac402c8cd292b8a03482574915d1a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
                        php composer-setup.php
                        php -r "unlink('composer-setup.php');"
                        sudo mv composer.phar /usr/local/bin/composer
                    """
                }
            }

        }

        stage("Installation"){
            when { expression { 'install' in modes } }

            stages{

                stage("Clean installation"){
                    when { expression { 'clean_install' in themes } }

                    steps{
                        script{
                            sh """
                                composer create-project 'drupal/recommended-project:^${Drupal_version}' drupal${Drupal_version}
                                cd drupal${Drupal_version}
                            """
                        }
                    }
                }

                stage("Dark theme installation"){
                    when { expression { 'dark_theme' in themes } }

                    steps{

                        checkout([$class: 'GitSCM',
                            branches: [[name: "master"]],
                            doGenerateSubmoduleConfigurations: false,
                            extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'drupal']], 
                            gitTool: 'Default',
                            submoduleCfg: [],
                            userRemoteConfigs: [[url: 'https://bitbucket.org/itmlgr/drupal_install.git', credentialsId: 'bitbucket_password']]
                        ])

                        dir("${WORKSPACE}/drupal"){
                            script{
                                sh """
                                    ls -la 
                                    cp sample.env .env  
                                    config_file="${WORKSPACE}/drupal/config/vhosts/configv${Drupal_version}/config${Drupal_version}.conf"
                                    hostname=InetAddress.localHost.hostAddress
                                    echo "\$hostname"
                                    sed -i 's/_CUSTOM_HOST_NAME_/dawn.itml.gr/' \$config_file 
                                    ./scripts/drupal-installer.sh -n ${Project_name} -dv ${Drupal_version} -pv ${PHP_version} -y
                                """
                            }
                        }
                    }
                }
            }

        }

        stage("Testing for upgrade"){
            when { expression { 'upgrade' in modes } }

            stages{

                stage("Test for upgrade"){
                    steps{
                        dir("${WORKSPACE}/drupal"){
                            script{
                                sh """
                                    composer show drupal/core | grep versions
                                    composer require --dev drupal/core-dev:[copy version above] --update-with-all-dependencies
                                    composer require drupal/upgrade_status
                                    upgrade_status:analyze (us-a)
                                    ./www/vendor/bin/drush en upgrade_status -y
                                """
                            }
                        }
                    }
                }

            }

        }

    }

    post{
        success{
            echo "The pipeline finished successfully!"
        }
    }

}