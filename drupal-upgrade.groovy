def modes=[]

node('devops') {
    modes = params["MODES"].split(",")
}

pipeline{

    agent { label 'devops' }

    parameters{
        checkboxParameter(name: 'MODES', format: 'JSON', pipelineSubmitContent: '{"CheckboxParameter": [{"key": "Update","value": "update"},{"key": "Test updated version","value": "test"}]}', description: 'Choose the modes in which the pipeline will run.')
        string(name: "Project_name", defaultValue: "drupal", description: "The name of the project, if upgrade is selected." )
        string(name: "Path_to_Drupal_Site", defaultValue: "", description: "The full path of to the drupal site." )
        string(name: "Repository_url", defaultValue: "", description: "The url of cloning the sites's repository." )        
        string(name: "Harbor_Image_Version", defaultValue: "", description: "The version of the image to be uploaded, if upgrade is selected." )
    }

    // environment{

    // }

    stages{

        stage("Update"){
            when { expression { 'update' in modes } }

            stages{

                stage("Update drupal image"){

                    steps{

                        checkout([$class: 'GitSCM',
                            branches: [[name: "master"]],
                            doGenerateSubmoduleConfigurations: false,
                            extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "${Project_name}"]], 
                            gitTool: 'Default',
                            submoduleCfg: [],
                            userRemoteConfigs: [[url: "${Repository_url}", credentialsId: 'bitbucket_token']]
                        ])

                        dir("${WORKSPACE}/${Project_name}"){
                            script{
                                sh """
                                    mv docker-compose.yml docker-compose.new.yml
                                    cp -r ./scripts docker-compose.new.yml sample.env $Path_to_Drupal_Site
                                    cd $Path_to_Drupal_Site
                                    cp sample.env .env
                                    ls -la
                                    chmod +x scripts/fullfill_reqs.sh
                                    ./scripts/fullfill_reqs.sh -n ${Project_name} -y
                                """
                            }
                        }
                    }
                }
            }

        }

        stage("Testing updated version"){
            when { expression { 'test' in modes } }

            stages{

                stage("Testing"){
                    steps{
                        script{
                            sh '''
                                docker exec -t ${Project_name}-webserver bash -c "cd /var/www/html && ./composer.phar show drupal/core | grep versions && \
                                                                                  ./vendor/bin/drush pm:security"
                            '''
                        }
                    }
                }

            }

        }

        stage("Upload tested version to Artifactory."){
            when { 
                allOf {
                    expression { 'update' in modes }
                    expression { 'test' in modes }
                }
            }
            stages{

                stage("Upload"){
                    steps{
                        withCredentials([usernamePassword(
                            credentialsId: 'harbor-jenkins-user',
                            usernameVariable: 'USER',
                            passwordVariable: 'PASS'
                        )]) {
                            sh '''
                                docker login --username ${USER} --password ${PASS} https://harbor.devops2.itml.tech
                                docker push https://harbor.devops2.itml.tech/testimages/${project_name}-image:${Harbor_Image_Version}
                            '''
                        }

                    }
                }

            }
        }

    }

    post{
        always{
            dir("${WORKSPACE}/${Project_name}"){
                script{
                    sh 'docker-compose down -v'
                }
            }
            // cleanWs deleteDirs: true
        }
        success{
            echo "The pipeline finished successfully!"
        }
    }

}